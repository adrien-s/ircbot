#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Markov Chain statistic-based talk :D"""

import data.Markov_2 as Markov
import random
import re

bot = None
talk = 0

def init(botInstance):
    """Inits the msgTrigger module"""
    global bot

    Markov.initDb()

    bot = botInstance

    bot.modules.modules["01-simpleCommand"].registerCommand(cmdGraph, "graph")
    bot.modules.modules["01-simpleCommand"].registerCommand(cmdRandWalk, "randwalk")
    bot.modules.modules["01-simpleCommand"].registerCommand(cmdTalk, "talk")
    bot.modules.modules["01-simpleCommand"].registerCommand(cmdRandomness, "randomness")
    bot.modules.modules["01-simpleCommand"].registerCommand(cmdFlood, "flood")
    bot.modules.modules["01-simpleCommand"].registerCommand(cmdShut, "shut")
    bot.irc.hooks["PRIVMSG"].append(talkCheck)

def cmdRandWalk(data, opts=[]):
    """Sets if random walk or not.
    randwalk {on,off}"""

    if len(opts) >= 1 and ["on", "off"].__contains__(opts[0].lower()):
        if opts[0].lower() == "on":
            Markov.cfg["randomWalk"] = True
        else:
            Markov.cfg["randomWalk"] = False
        Markov.saveCfg()

def cmdGraph(data, opts=[]):
    """Print graph info
    graph: infos on graph"""
    bot.irc.msg("Word graph contains %d nodes" % len(Markov.chainDic), data["tgt"])
    bot.irc.msg("Word dictionary contains %d entries" % len(Markov.wordDic), data["tgt"])
    bot.irc.msg("Word graph has a depth of %d" % Markov.cfg["chainWindowSize"], data["tgt"])
    if Markov.cfg["randomWalk"]:
        bot.irc.msg("Word graph is walked randomly", data["tgt"])
    else:
        bot.irc.msg("Word graph is walked randomly, respective to each sequence frequency", data["tgt"])        
    return

def cmdFlood(data, opts=[]):
    """Lets the bot talk, without taking talkability parameter into account."""
    global talk
    talk = 2

def cmdTalk(data, opts=[]):
    """Lets the bot talk
    talk [talkability]"""
    global talk
    talk = 1
    if len(opts) > 0:
        Markov.cfg["talkability"] = int(opts[0])
        Markov.saveCfg()

def cmdRandomness(data, opts=[]):
    """Lets the bot say random things when it has no clue what to answer.
    randomness [0-100]"""
    if len(opts) > 0:
        Markov.cfg["randomness"] = int(opts[0])
        Markov.saveCfg()

def cmdShut(data, opts=[]):
    """Makes the bot be quiet"""
    global talk
    talk = 0

def talkCheck(evt):
    """Hook for the event PRIVMSG"""

    user = evt[0][1:].split("!")[0]
    tgt = evt[2]
    txt = (" ".join(evt[3:])[1:]).lower()

    if tgt==bot.cfg["nick"]:
        tgt = user

    # Don't analyze commands
    if txt.split()[0][0] == bot.modules.modules["01-simpleCommand"].moduleData["cmdChar"]:
        return
    
    # Do not analyze urls.
    urls = re.findall(' ?https?://\S+', txt)
    for url in urls:
        txt = txt.replace(url, "")

    # No usertag neither
    usertag = re.match('^<[^>]+> ', txt)
    if usertag!=None:
        txt = txt.replace(usertag.group(), "")

    if (talk==1 and random.randint(0, 100)<=Markov.cfg["talkability"]) or talk == 2:
        # Last conditions tells the bot to just shut up if he doesn't know what to say next (unless we're in mode 2, then he _must_ talks)
        msg = Markov.computeAnswer(txt)
        if msg!="":
            bot.irc.msg(msg, tgt)
    else:
        Markov.analyzeSentence(txt)
