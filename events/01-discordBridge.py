#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Cross-talks between #IRC and @discord."""

import os
import re
import time

bot = None

spoolBaseDir='/tmp/spool'
spoolDir='/tmp/spool/freenode'

ircFormatRegExp = {
    "bold": re.compile("(.*?)(|)"),
    "italic": re.compile("(.*?)(|)")
}
ircFormatOutput = {
    "both": "\\1",
    "bold": "\\1",
    "italic": "\\1"
}

mkdFormatRegExp = {
    "both": re.compile("\\*\\*\\*(.*)?\\*\\*\\*"),
    "bold": re.compile("\\*\\*(.*)?\\*\\*"),
    "italic": re.compile("\\*(.*)?\\*"),
}
mkdFormatOutput = {
    "bold": "**\\1**",
    "italic": "*\\1*"
}

def init(botInstance):
    """Inits the module"""
    global bot

    bot = botInstance

    bot.irc.hooks["PRIVMSG"].append(ircmsg)
    bot.irc.hooks["JOIN"].append(ircjoin)
    bot.irc.hooks["PART"].append(ircpart)
    bot.irc.hooks["QUIT"].append(ircquit)

    if not os.path.exists(spoolDir+"/in"):
        if not os.path.exists(spoolDir):
            if not os.path.exists(spoolBaseDir):
                os.mkdir(spoolBaseDir, mode=int('1750', 8))
            os.mkdir(spoolDir, mode=int('1750', 8))
        os.mkdir(spoolDir+"/in", mode=int('1750', 8))
    if not os.path.exists(spoolDir+"/out"):
        os.mkdir(spoolDir+"/out", mode=int('1750', 8))

def poolAdd(channel, author, message):
    for k in ircFormatRegExp:
        message = ircFormatRegExp[k].sub(mkdFormatOutput[k], message)

    outChannelDir='{s}/out/{c}'.format(s=spoolDir, c=channel)
    if os.path.exists(outChannelDir):
        for target in os.listdir(outChannelDir):
            with open('{s}/{t}:{a}'.format(s=outChannelDir+"/"+target, t=int(time.time()), a=author), "w") as f:
                f.write(message)

def getMessage():
    for chan in os.listdir('{s}/in'.format(s=spoolDir)):
        l = sorted(os.listdir('{s}/in/{c}'.format(s=spoolDir, c=chan)))
        if len(l)>0:
            break
    if len(l) == 0:
        return None

    spoolMessageFileName = l[0]
    ts = spoolMessageFileName.split(":")[0]
    channel = chan
    author = ":".join(spoolMessageFileName.split(":")[1:]).replace(" ", "_")
    with open('{s}/in/{c}/{f}'.format(s=spoolDir, c=channel, f=spoolMessageFileName), 'r') as f:
        message = f.read()
    os.unlink('{s}/in/{c}/{f}'.format(s=spoolDir, c=channel, f=spoolMessageFileName))

    for k in mkdFormatRegExp:
        message = mkdFormatRegExp[k].sub(ircFormatOutput[k], message)

    return {'channel':channel, 'author':author, 'message':message}

def ircmsg(evt):
    author = evt[0][1:].split("!")[0]
    channel = evt[2]
    message = ' '.join(evt[3:])[1:]
    if channel[0]!='#':
        return
    poolAdd(channel, author, message)

def ircjoin(evt):
    author = evt[0][1:].split("!")[0]
    channel = evt[2]
    if channel[0]!='#':
        return
    poolAdd(channel, author, 'joined IRC')

def ircpart(evt):
    author = evt[0][1:].split("!")[0]
    channel = evt[2]
    message = ' '.join(evt[3:])[1:]
    if channel[0]!='#':
        return
    poolAdd(channel, author, 'parted ({})'.format(message))

def ircquit(evt):
    author = evt[0][1:].split("!")[0]
    channel = evt[2]
    message = ' '.join(evt[3:])[1:]
    if channel[0]!='#':
        return
    poolAdd(channel, author, 'has quit ({})'.format(message))
